var fs = require('fs');

var getAll = function(callback) {
    fs.readFile('promos.json', 'utf8', function(err, fileData){
        if(err) {
            callback.failure(err);
            return;
        }
        var jsonData = JSON.parse(fileData);
        callback.success(jsonData.data);
    });
};

var getById = function(promoId, callback) {
    getAll({
        success: function(models) {
            var foundModel = models.find(function(item){
                return item.id === promoId
            });
            callback.success(foundModel);
        },
        failure: function(err) {
            callback.failure(err);
        }
    });
};

module.exports = {
    getAll: getAll,
    getById: getById
}